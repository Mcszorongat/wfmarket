# -*- coding: utf-8 -*-

import pytest
from wfmarket.skeleton import fib

__author__ = "Mcszorongat"
__copyright__ = "Mcszorongat"
__license__ = "mit"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
