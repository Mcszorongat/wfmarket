echo -- Setting up virtual environment...
py -m venv .venv
echo -- Activating vEnv...
call .\.venv\Scripts\activate.bat
echo -- Updating pip...
py -m pip install --upgrade pip
echo -- Pip install wheel and pylint
pip install wheel
pip install pylint --upgrade
echo -- Pip install pyscaffold
pip install pyscaffold
pause